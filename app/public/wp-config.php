<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'fABcSfrQ/Ekx6ZDLgDGAigUhyoXRau0RQeYOSTQVnlN8LOeDM4qOPqqR/kxd7K20WOWu4aYzQ+euBCTfvFnXmQ==');
define('SECURE_AUTH_KEY',  'u3dUOGHmLmfzN1H5YJN8pVLMJSeN8CjBrBn3Oc2cLkrtqLilHqAjoaiQgFGiVFvEF//u28NKUa/fPYeJmIze9g==');
define('LOGGED_IN_KEY',    'qh8ofQdk02m6dXCIxYpLCO6JXCQlfXNjreyCsd+XcNNhWgGN9rZLoBRaqCLaqG/eh+/k4zTbaemIpbx23kugCw==');
define('NONCE_KEY',        'TyNHGgTSA4LXvaaNumUXhC4WjoocuU+QWusKwJCXuGDUToHWL3F/aEOiQHao7aquYNCc2s0Yd5+o8hYMVZyXJQ==');
define('AUTH_SALT',        'EpEvReqfFOXDCJnXU8vz5IY1PvTO4ZdDBYBN/k/oJmB9cpc4xw15XnWkh0BzLJnLimN72kmiyfWfQiH3st3/XQ==');
define('SECURE_AUTH_SALT', '89J/GfH5NfZGmaZ3JPo0019ODUJSwWT/OLii88oq2hD/4Aclfb6QAZw/T8uyl7Iz1JsWXLuuBPoD32fFgCR2NQ==');
define('LOGGED_IN_SALT',   '3SrQ3oZFjdqBlvBFwwz7kRucydU0sVfNaBi5NU3gFeuOke5uFBfKJH6SPtb836QjiUviwn2q0aIY1aN9RgU5lg==');
define('NONCE_SALT',       'iZeSWVkAk/h8xTR7m8vBHCFD3C0V/wa9IHyFoOQFExNSleABDtB8kwRrKn0d5Jn8SdI/oD5eEUJWPecjyqQVsw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
