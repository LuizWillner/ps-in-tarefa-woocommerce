NERD{IN} é uma plataforma geek para venda de artigos de colecionador do mundo nerd! Compre os action-figures, estatuetas e brinquedos mais estimados que todo nerd quer ter! Vendemos itens da Marvel, DC, universo Star Wars, Senhor dos Anéis, Game of Thrones e muito mais! Só aqui você encontra os melhores preços e os produtos de melhor qualidade, com certificação positiva reconhecida internacionalmente. Não perca tempo!

Nerd{IN}: Nerdice é a nossa religião!
